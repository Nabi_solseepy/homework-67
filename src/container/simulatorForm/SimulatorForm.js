import React, {Component} from 'react';
import {connect} from 'react-redux'
import './SimulatorFrom.css'


const numbers = ['1', '2', '3', '4', '5','6', '7','8','9', '0'];

class SimulatorForm extends Component {
    render() {
        return (
            <div className="form-add">
                {this.props.correct ? <div className="Access">Access Granted</div>: null}
                <div className={"form " .concat(this.props.bgError)}>{this.props.string}</div>

                {numbers.map(number => (
                    <button onClick={() => this.props.addNumber(number)}>{number}</button>
                ))}
             <button onClick={this.props.removeNamber}>&lt;</button>
                <button onClick={this.props.checkCombination}>E</button>
            </div>
        );
    }
}


const mapStateToProps = state => {
    return{
        string: state.string,
        code: state.code,
        correct: state.correct,
        bgError: state.bgError
    }
};
const mapDispatchToProps = dispatch => {
    return{
        addNumber: num => dispatch({type: 'ADD_NUMBER', num}),
        removeNamber: () => dispatch({type: 'REMOVE_NOMBER'}),
        checkCombination: () => dispatch({type: 'CKECK_COMBINATION'})
    }
};

export default connect(mapStateToProps,mapDispatchToProps )(SimulatorForm);