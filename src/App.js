import React, { Component } from 'react';

import './App.css';
import SimulatorForm from "./container/simulatorForm/SimulatorForm";

class App extends Component {
  render() {
    return (
      <div className="App">
        <SimulatorForm/>
      </div>
    );
  }
}

export default App;
